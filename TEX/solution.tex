\section{Memory optimization light (MemOpLight)}
\label{sec_solution}
\subsection{MemOpLight's functioning}
It is difficult to do better with the static limits and the limited information available to the kernel.
The kernel only has access to low-level metrics such as the CPU cycles used or the I/O bandwidth.
Theses are, at best, proxies for application level quality of service, and are not representative of the needs of modern applications.

Instead, we propose to base memory reclamation on performance metrics perceived by the application.
The container itself declares its current level of satisfaction, according to its own, application-specific metrics (\textit{e.g.} latency for a web server, frame rate for a streaming application, etc.), compared to some performance objectives.
We associate a traffic light color to different states of performance:
\begin{itemize}
	\item GREEN: The container has reached its maximum level and would not benefit from more resources.
	For instance, a web server has no requests waiting.
	\item YELLOW: The container is satisfied, but it would do better with more resources.
	For example, a web server has pending requests in a queue but answered requests were dealt under a given latency.
	\item RED: The container is not satisfied, \textit{e.g.} a web server is not able to answer requests under a certain latency.
\end{itemize}
Containers begin their execution being RED while the probe does not indicate the contrary.

Our approach is based on three components.

\paragraph{User-kernel communication}
We develop a communication mechanism between user and kernel, so a container can communicate its state of performance to the kernel.
The Linux kernel provides multiple mechanism for this purpose (\textit{e.g.}, syscall, ioctl,  sysfs, socket, \textit{etc.}).
We choose the \texttt{sysfs} because it is less intrusive for the kernel and has a file-oriented API which is easy to use by users.

\paragraph{Probe}
We need a second component: a probe specific to the application.
A container is equipped with an applicative probe, which indicates its color, based on some application-specific objective.
The probe can consist in modifications of the application or a script that collects information about the application.
For \texttt{mysql}, the probe is a script that reads the database log and analyzes the request latency.

In our prototype, the probe compares the transaction latency of \texttt{mysql}, which executes inside the container, to some SLO and informs Linux through \texttt{sysfs} every second.
Specifically, a container with high throughput declares itself green if its throughput equals the SLO and has no transactions waiting.
With low throughput, it is green if it handles $\approx 200$ t/s.
If it cannot respect its SLO, it is red.
Otherwise, if it has transactions waiting, it is yellow.

\paragraph{MemOpLight}
Our third component is an algorithm that executes when memory is scarce.
MemOpLight extends the Linux page frame reclaim algorithm (PFRA) \cite{bovet_understanding_2006}.
When a container needs memory, it takes it from the free physical pages.
When the number of free physical pages is under a given threshold, the PFRA starts.
The PFRA activates when a memory threshold is reached, \textit{i.e.} free physical memory becomes scarce.
It activates to reclaim memory and bring the free physical memory above the threshold.
With containers, this algorithm has two parts.
The first one consists in recycling memory from one container (local reclaim) while the other reclaims memory from all containers (global reclaim).
It reclaims memory from containers based on their colors, there are three cases:

\begin{description}
	\item[There is at least one red container:] The SLO is not respected globally, there are two cases :
	\begin{itemize}
		\item If there is green or yellow containers, they will be reclaimed.
		\item Otherwise there are only red containers, the \texttt{soft} limit activates and reclaims red containers.
	\end{itemize}
	\item[There is no red container but at least one yellow container:] The SLO is respected globally but containers can have better performance, there is also two cases:
	\begin{itemize}
		\item If at least one green container exists, they are reclaimed.
		\item Otherwise there are only yellow containers, the \texttt{soft} limit reclaims the yellow containers.
	\end{itemize}
	\item[All containers are green:] \textit{We live in the best of all possible worlds}, we let the \texttt{soft} limit reclaims memory if it has to do it.
\end{description}

Besides the technical details not discussed in the article, but commented in the code, we make two important choices in our implementation of the above algorithm.

The first one is to reclaim both green and yellow containers when there is at least one red.
By doing so, we hope to maximize the number of satisfied containers quickly.

The other choice is to reclaim only some fraction of the physical memory footprints of containers periodically.
We set this proportion to 2\% each and period to each second to smooth the transitions from green/yellow to red.
The goal is to converge toward the most effective configuration while avoiding oscillations between red and green due to MemOpLight.
Indeed, if a container declares itself as green or yellow, this must mean that its WS fits into its current physical memory allocation.

This mechanism tends to adapt the amount of memory to what is required for each container to be satisfied.
Memory reclaimed can be used by other containers to improve their own performance and satisfaction.

We implemented MemOpLight in existing Linux kernel code.
We modified Linux code to call MemOpLight once per second when there is memory pressure.
If MemOpLight fails to reclaim memory, the \texttt{soft} limit mechanism activates.
Our modifications amount to $\approx 400$ lines of code.

\subsection{Generalization of probe}
\label{subsec_probe}
Our probe can be related to the Monitor and Analyze phase of the MAPE loop, while Plan and Execution phase consist in our modifications to Linux kernel \cite{kephart_vision_2003}.
So, we can generalize our concept of probe.
Indeed, the probe executes 3 actions:
\begin{enumerate}
	\item It collects information about the application.
	\item It then compares the information to a SLO, which was given by container's owner at its startup.
	This comparison states about the application performance.
	\item Finally, it communicates the application state of performance to Linux kernel.
\end{enumerate}
It is easy to implement a probe for a web server or a streaming application.
The probe will read the request response time inside the web server log.
It will then compare the read response time with the SLO of the web server.
If the response time is higher than SLO, then web server performance are RED, otherwise they are GREEN.
We can design another probe, with this time 3 levels of performance, for a streaming application.
If the streaming application sustains the SLO frame rate with high video quality, its state of performance is GREEN.
But if it provides frames at the SLO frame rate but with a lower quality, its state will be YELLOW.
Otherwise, \textit{i.e.} if it cannot respect the SLO, its performance are RED.
Nonetheless, the probe must be as precise as possible since clients are charged for used memory each second \cite{amazon_pricing_nodate, google_vm_2020}.
The probe also presents no overhead since it writes a file each second.