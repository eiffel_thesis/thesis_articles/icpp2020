\section{Evaluation}
\label{sec_evaluation}
\subsection{MemOpLight with two containers}
\label{subsec_evaluation_two_containers}
MemOpLight is designed to consolidate memory while ensuring memory performance isolation.
To verify this, we run again the experiment from Section \ref{subsec_problem_soft}, this time enabling MemOpLight.
We plot the results in Figure \ref{fig_mechanism}.

\step{1}{h}{h}, \step{3}{h}{i} and \step{5}{i}{h} in Figure \ref{fig_transactions_mechanism} follow the same pattern as the same phases in Figure \ref{fig_transactions_soft}.
Figure \ref{fig_memory_mechanism} shows that, like Figure \ref{fig_memory_soft}, when both containers receive high or intermediate load, their memory footprints follow their \texttt{soft} limits (\step{1}{h}{h}, \step{3}{h}{i} and \step{5}{i}{h}).
A has then a larger footprint than B, this shows that MemOpLight ensures memory performance isolation.
Moreover, in these phases, performance of containers A and B are better than with \texttt{max} and \texttt{soft} limits.
On average, container A answers to 1351, 1362 and 1370 transactions per second compared to 1290, 1314 and 1358 with the \texttt{max} limit; this represents an increase of respectively 4.7\%, 3.7\% and 0.9\%.
With MemOpLight, container B handles almost 933, 999 and 1036 transactions per second while it process only 894, 968 and 978 transactions per second with the \texttt{max} limit; resulting in an increase of 4.4\%, 3.2\% and 5.9\%.
These increases can be explained because MemOpLight permits converging to a balance where containers stop stealing memory each other.

Like \step{6}{s}{h} in Figure \ref{fig_memory_soft}, when A stops completely, there is no more memory pressure, B's footprint grows and permits it to increase its performance to the maximum throughput.

MemOpLight dynamic nature brings a real benefit in \step{2}{h}{l}.
During this phase, B receives low load and A a high one.
As shown in Figure \ref{fig_memory_mechanism}, there is a memory transfer from container B to container A.
Since container B has no requests in its queue, its memory is reclaimed until finding the threshold where it can still answer to 200 t/s.
The applicative feedback permits finding this memory configuration without previous static analysis.
MemOpLight maximizes memory consolidation so container A's throughput equals its SLO.

\begin{table*}
	\centering

	\caption{Median value of measured throughput in each phase of each experiment (in t/s rounded to the nearest integer)}
	\label{table_sumup}

	\begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c}
		\hline
		\diagbox{Transactions}{Phases} & \multicolumn{2}{c|}{\step{1}{h}{h}} & \multicolumn{2}{c|}{\step{2}{h}{l}} & \multicolumn{2}{c|}{\step{3}{h}{i}} & \multicolumn{2}{c|}{\step{4}{l}{l}} & \multicolumn{2}{c|}{\step{5}{i}{h}} & \step{6}{s}{h}\\
		\hline
		Container & A & B & A & B & A & B & A & B & A & B & B\\
		\hline
		Input load & 2000 & 2000 & 2000 & 200 & 2000 & 1500 & 200 & 200 & 1500 & 2000 & 2000\\
		\hline
		Limits not set (Section \ref{subsec_problem_without}) & 1053 & \textbf{1043} & 1374 & 196 & 1054 & \textbf{1168} & 195 & 197 & 1195 & \textbf{1145} & \textbf{1751}\\
		\texttt{Max} limit (Section \ref{subsec_problem_max}) & 1290 & 894 & 1411 & 196 & 1314 & 968 & 196 & 660 & 1358 & 978 & 962\\
		\texttt{Soft} limit (Section \ref{subsec_problem_soft}) & 1268 & 879 & 1423 & 197 & 1299 & 969 & 196 & 794 & 1360 & 970 & 974\\
		MemOpLight (Section \ref{subsec_evaluation_two_containers}) & \textbf{1351} & 933 & \textbf{1670} & 195 & \textbf{1362} & 999 & 196 & 197 & \textbf{1370} & 1036 & 1723\\
	\end{tabular}
\end{table*}

Table \ref{table_sumup} summarizes all the previous measurements.
B has lower performance with MemOpLight and would have better performance with no limits set.
This behavior is normal, since in the latter case there is no memory performance isolation.
Nonetheless, this is not expected in cloud since a client could have paid more than another so one client would have a better offer than the other.

In summary, MemOpLight increases performance of containers by redistributing memory following their needs.

\subsection{MemOpLight with eight containers}
To confirm that MemOpLight adapts to application loads thanks to applicative feedback, we now run an experiment with 8 containers.
Along the phases, which each lasts for 120 seconds, containers receive high, intermediate or low load.
The container loads were chosen randomly to cover different cases and put MemOpLight in difficulty.

The scenario and its characteristics are described in Table \ref{table_load}.
The first column is SLO.
The second one shows the values for \texttt{max} and \texttt{soft} limits.
The other columns describe the load at each phase of the experiment.
Throughput can have multiple values:
\begin{itemize}
	\item High: The container receives 2500 t/s.
	Black cells, in Table \ref{table_load}, depicts phases where containers receive high load.
	\item Intermediate : The container receives its SLO $\pm 5\%$ t/s.
	In Table \ref{table_load}, gray and light gray cells show phases with intermediate load.
	\item Low: The container sustains only 200 t/s.
\end{itemize}

The different phases of the scenario can be grouped to form 3 different periods.
In \stepeight{1} and \stepeight{2},
In \stepeight{3} and \stepeight{4}, the containers receive low loads.
During \stepeight{5} to \stepeight{9}, the system is moderately busy but container loads increase over time.

\begin{table*}
	\centering

	\caption{Throughput in each phase of experiment (in transactions per second)}
	\label{table_load}

	\begin{tabular}{l|c||c||c|c|c|c|c|c|c|c|c}
		\hline
		Containers & SLO (t/s) & Limit (if set) & \stepeight{1} & \stepeight{2} & \stepeight{3} & \stepeight{4} & \stepeight{5} & \stepeight{6} & \stepeight{7} & \stepeight{8} & \stepeight{9}\\
		\hline
		A & 1800 & \SI{1400}{\mega\byte} & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey_200} 1710 & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{blue_grey} 1890 & 200 & 200 & 200 & \cellcolor{black} \textcolor{white}{2500}\\
		B & 1600 & \SI{1000}{\mega\byte} & \cellcolor{blue_grey_200} 1520 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{blue_grey_200} 1520 & \cellcolor{black} \textcolor{white}{2500} & 200\\
		C & 1400 & \SI{800}{\mega\byte} & \cellcolor{blue_grey_200} 1330 & \cellcolor{blue_grey} 1470 & 200 & \cellcolor{blue_grey} 1470 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey_200} 1330 & \cellcolor{blue_grey} 1470 & 200 & \cellcolor{blue_grey} 1470\\
		D & 1400 & \SI{800}{\mega\byte} & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey} 1470 & 200 & 200 & 200 & \cellcolor{blue_grey} 1470 & 200 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{black} \textcolor{white}{2500}\\
		E & 1200 & \SI{600}{\mega\byte} & \cellcolor{blue_grey_200} 1140 & \cellcolor{blue_grey_200} 1140 & 200 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey} 1260 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{black} \textcolor{white}{2500} & 200\\
		F & 1200 & \SI{600}{\mega\byte} & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey} 1260 & 200 & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{black} \textcolor{white}{2500} & \cellcolor{blue_grey} 1260 & \cellcolor{blue_grey} 1260 & \cellcolor{black} \textcolor{white}{2500}\\
		G & 1000 & \SI{400}{\mega\byte} & \cellcolor{black} \textcolor{white}{2500} & 200 & \cellcolor{black} \textcolor{white}{2500} & 200 & 200 & 200 & 200 & \cellcolor{blue_grey_200} 950 & \cellcolor{blue_grey_200} 950\\
		H & 800 & \SI{400}{\mega\byte} & \cellcolor{blue_grey} 840 & \cellcolor{black} \textcolor{white}{2500} & 200 & 200 & \cellcolor{blue_grey_200} 760 & 200 & \cellcolor{blue_grey_200} 760 & \cellcolor{blue_grey_200} 760 & \cellcolor{black} \textcolor{white}{2500}\\
	\end{tabular}
\end{table*}

In this experiment, we focus on the respect of SLO and the global performance.
Figure \ref{fig_eight_throughput} plots the average throughput of containers generated across 10 runs for different mechanisms.
The global throughput is depicted by the height of the bars while the different colors indicate the throughput of each container.
Hatched colors emphasize throughput that is above SLO.

Figure \ref{fig_eight_color} focuses on the respect of the SLO.
It plots the colors of containers over time during the sixth run of our experiment.
To plot this Figure, we activate the probes for all mechanisms.
As we discussed in Section \ref{subsec_probe}, the probe has no overhead.

\begin{figure}
	\centering

	\subfloat[Limits not set]{
		\includegraphics[scale = .50]{default_stacked_transactions.pdf}

		\label{fig_eight_default}
	}

	\subfloat[\texttt{Max} limits]{
		\includegraphics[scale = .50]{max_stacked_transactions.pdf}

		\label{fig_eight_max}
	}

	\subfloat[\texttt{Soft} limits]{
		\includegraphics[scale = .50]{soft_stacked_transactions.pdf}

		\label{fig_eight_soft}
	}

	\subfloat[MemOpLight]{
		\includegraphics[scale = .50]{mechanism_stacked_transactions.pdf}

		\label{fig_eight_mechanism}
	}

	\caption{Average throughput of eight containers during each phase with different mechanisms}
	\label{fig_eight_throughput}
\end{figure}

\begin{figure}
	\centering

	\subfloat[Limits not set]{
		\includegraphics[scale = .5]{default_color.pdf}

		\label{fig_eight_color_default}
	}

	\subfloat[\texttt{Max} limits]{
		\includegraphics[scale = .5]{max_limit_color.pdf}

		\label{fig_eight_color_max}
	}

	\subfloat[\texttt{Soft} limits]{
		\includegraphics[scale = .5]{soft_limit_color.pdf}

		\label{fig_eight_color_soft}
	}

	\subfloat[MemOpLight]{
		\includegraphics[scale = .5]{mechanism_color.pdf}

		\label{fig_eight_color_mechanism}
	}

	\caption{Containers' colors during the 5th run with different mechanisms}
	\label{fig_eight_color}
\end{figure}

In \stepeight{1} and \stepeight{2}, the system is overloaded since almost all containers receive high or intermediate loads.
The global throughput is lower with limits not set, \textit{e.g.} 836453 transactions (t.) compared to 895317.8 t. and 898728 t. for \texttt{max} and \texttt{soft} limits, because containers are fighting for memory so it is used ineffectively.
For these phases, MemOpLight achieves the best global throughput (927651 t. in \stepeight{1} and 937413 t. in \stepeight{2}).
Indeed, with MemOpLight, once containers reach a memory balance they stop fighting for memory.
Even containers which have less memory have better performance because they use it effectively.

\stepeight{3} is a transition phase between highly and lowly loaded.
This phase shows if mechanisms are able to adapt to this change.
During this phase, \texttt{max} and \texttt{soft} limits permit better performance than limits not set in \stepeight{3} (779570 t. and 762521 t. compared to 729834 t.).
As depicted in Figure \ref{fig_eight_mechanism}, MemOpLight permits a better global throughput (788909), thanks to its dynamic behavior.
Figure \ref{fig_eight_color_mechanism} shows that MemOpLight permits to containers to converge faster to their SLO.

In \stepeight{4}, containers continue to receive low loads.
In this phase, the \texttt{max} and \texttt{soft} limits activate so they block memory consolidation and impede container performance (583156 t. and 580914 t. compared to 714737 t. without limits set).
MemOplight offers the same performance as with limits not set (723084 t. \textit{vs.} 714737 t.).
With MemOplight, all containers are satisfied, since there are only green containers in \stepeight{4} in Figure \ref{fig_eight_color_mechanism}.
It also permits to more containers to exceed their SLO since hatched zones are bigger.

In \stepeight{5} to \stepeight{9}, the system is moderately busy but container loads increase over time.
Figure \ref{fig_eight_color_mechanism} shows that MemOpLight maximizes container satisfaction.
Moreover, at the beginning of each phase, we can see that containers reach satisfaction more quickly thanks to the dynamic applicative feedback.
During these phases, note that peaks of red can be explained because the benchmark offers an average load, not a constant one, we discuss this in the conclusion.

This experiment shows that MemOpLight permits consolidation and isolation as shown in Table \ref{table_sumup_eight} and Table \ref{table_sumup_eight_percent}.
Table \ref{table_sumup_eight} presents throughput for each container with all the tested mechanisms.
One can see that MemOpLight supports better performance for five of the eight containers.
Where MemOpLight performs worse, the difference is only about 0.6\%, 2.3\% and 1.9\% for container B, G and H compared to the experiment with limits not set.
Overall, MemOpLight reaches 122.6 millions t. which is 8.9\% better than with limits not set.
Table \ref{table_sumup_eight_percent} shows fraction of time where containers are satisfied.
This time is the sum of yellow and green time.
MemOpLight maximizes the number of satisfied containers.
It also increases the time passed satisfied throughout the whole experiment from 44\% to 57\%.
Thus, MemOpLight enhances efficiency, since it improves performance with the same hardware.

\begin{table*}
	\centering

	\caption{Total containers' throughputs (in million of t/s with different mechanisms, averaged over 10 runs)}
	\label{table_sumup_eight}

	\begin{tabular}{l|cccccccc|c}
		\hline
		\diagbox{Mechanism}{Containers} & A & B & C & D & E & F & G & H & Total\\
		\hline
		No limits set & 15.2 & 14.5 & 17.3 & 11.9 & 18.7 & 15.9 & \textbf{8.8} & \textbf{10.3} & 112.6\\
		\texttt{Max} limit & 13.6 & 13.8 & 13.8 & 8.5 & 14.9 & 9.9 & 4.6 & 6.8 & 85.9\\
		\texttt{Soft} limit & 17.1 & \textbf{17.4} & 17.6 & 12.6 & 17.5 & 13.4 & 7.3 & 9.5 & 112.4\\
		MemOpLight & \textbf{17.5} & 17.3 & \textbf{18.4} & \textbf{13.7} & \textbf{20.2} & \textbf{16.8} & 8.6 & 10.1 & \textbf{122.6}\\
	\end{tabular}
\end{table*}

\begin{table*}
	\centering

	\caption{Fraction of time (in percent) where container is satisfied (\textit{i.e.} yellow or green) according to mechanisms (averaged over 10 runs)}
	\label{table_sumup_eight_percent}

	\begin{tabular}{l|cccccccc|c}
		\hline
		\diagbox{Mechanism}{Containers} & A & B & C & D & E & F & G & H & Total\\
		\hline
		No limits set & 42 & 23 & 8 & 35 & 69 & 39 & 62 & \textbf{71} & 44\\
		\texttt{Max} limit & \textbf{60} & 35 & 5 & 37 & 73 & 15 & 53 & 35 & 39\\
		\texttt{Soft} limit & 56 & 42 & 11 & 35 & 64 & 16 & 53 & 32 & 39\\
		MemOpLight & 58 & \textbf{44} & \textbf{42} & \textbf{52} & \textbf{76} & \textbf{53} & \textbf{67} & 67 & \textbf{57}\\
	\end{tabular}
\end{table*}

In summary, MemOpLight improves throughput and enables containers to be satisfied more often.